package no.uib.inf101.sem2.model.move;

public class HyperBeamMove extends Move {
    //Constructor
    public HyperBeamMove() {
        super(
            "Hyper-beam",
            "SPE",
            50, 
            150, 
            0,
            50, 
            5
        );
    }
    
}
