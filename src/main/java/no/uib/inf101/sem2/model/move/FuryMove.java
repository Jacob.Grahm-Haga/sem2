package no.uib.inf101.sem2.model.move;

public class FuryMove extends Move {
    //Constructor
    public FuryMove() {
        super(
            "Fury",
            "PHY",
            75,
            150, 
            0, 
            50, 
            5
        );
    }
    
}
