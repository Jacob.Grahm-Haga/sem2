package no.uib.inf101.sem2.model.move;

import java.util.Random;
import no.uib.inf101.sem2.model.pokemon.*;

public abstract class Move implements IMove {
    //Field vars
    String moveName;
    String moveType;
    int moveHitChance;
    int movePower;
    int moveVariance;
    int moveSpeed;
    int maxMoveCount;
    int moveCount;
    Random random;

    //Constructor
    public Move(String moveName, String moveType, int moveHitChance, int movePower, int moveVariance, int moveSpeed, int maxMoveCount){
        this.moveName = moveName;
        this.moveType = moveType;
        this.moveHitChance = moveHitChance;
        this.movePower = movePower;
        this.moveVariance = moveVariance;
        this.moveSpeed = moveSpeed;
        this.maxMoveCount = maxMoveCount;
        this.moveCount = maxMoveCount;
        this.random = new Random();
    }

    @Override
    public String getMoveName() {
        return moveName;
    }

    @Override
    public int getMoveHitChance() {
        return moveHitChance;
    }

    @Override
    public int getMovePower() {
        return movePower;
    }

    public int getMoveVariance() {
        return moveVariance;
    }

    @Override
    public int getMoveSpeed() {
        return moveSpeed;
    }

    @Override
    public int getMaxMoveCount() {
        return maxMoveCount;
    }

    @Override
    public int getMoveCount() {
        return moveCount;
    }

    @Override
    public String displayMoveString() {
        String result = (
            moveName + "(" + moveCount + "/" + maxMoveCount + ") [" + moveType + "]:\n" + 
            "PWR: " + movePower + " | SPD: " + moveSpeed + " | HIT%: " + moveHitChance + "%");
        return result;
    }

    @Override
    public String[] displayMove() {
        String[] result = {
            moveName + "(" + moveCount + "/" + maxMoveCount + ") [" + moveType + "]",
            "PWR: " + movePower + " | SPD: " + moveSpeed + " | HIT%: " + moveHitChance + "%"
        };
        return result;
    }

    @Override
    public String useMove(Pokemon target){
        if (moveCount != 0){
            moveCount -= 1;
            return (attack(target));
        }
        return "Out of moves";
    }

    @Override
    public String attack(Pokemon target){
        int hitNum = random.nextInt(101);
        if (hitNum <= (moveHitChance / 10)){ //Critical hit if hitNum under 10% of moveHitChance
            target.takeDamage(movePower * 2);
            return ("Critical hit! " + target.getName() + " takes " + movePower * 2 + " DMG");
        } else if (hitNum <= moveHitChance){ //Hit if hitNum under moveHitChance
            target.takeDamage(movePower);
            return ("Hit! " + target.getName() + " takes " + movePower + " DMG");
        } else {
            return ("Miss");
        }
        
    }


    
}
