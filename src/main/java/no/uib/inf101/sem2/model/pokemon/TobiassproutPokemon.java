package no.uib.inf101.sem2.model.pokemon;

import java.util.*;
import no.uib.inf101.sem2.view.*;

import no.uib.inf101.sem2.model.move.*;

public class TobiassproutPokemon extends Pokemon{

    public TobiassproutPokemon() {
        super(
            "Tobiassprout", 
            300, 
            100, 
            0, 
            100, 
            0, 
            100, 
            new ArrayList<>(Arrays.asList(
                new SurgeMove(), 
                new HyperBeamMove(), 
                new FuryMove(), 
                new KillCommandMove()
            )),
            Inf101Graphics.loadImageFromResources("/TobiassproutSprite.png")
        );
    }
    
}
