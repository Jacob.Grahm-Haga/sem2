package no.uib.inf101.sem2.model.pokemon;

import java.util.ArrayList;
import java.awt.image.BufferedImage;
import no.uib.inf101.sem2.model.move.*;

public interface IPokemon {

    /**
     * @return pokemon name as string
     */
    public String getName();

    /**
     * @return physical attack power (0-100) as int
     */
    public int getAttack();

    /**
     * @return special attack power (0-100) as int
     */
    public int getSpecialAttack();

    /**
     * @return speed (0-100) as int
     */
    public int getSpeed();

    /**
     * @return physical defense (0-100) as int
     */
    public int getDefense();

    /**
     * @return special defense (0-100) as int
     */
    public int getSpecialDefense();

    /**
     * @return current hit points as int
     */
    public int getCurrentHP();

    /**
     * @return max hit points as int
     */
    public int getMaxHP();

    /**
     * @return true if HP > 0, false if not
     */
    public boolean isAlive();

    /**
     * @return arrayList of moves
     */
    public ArrayList<Move> getMoves();

    /**
     * @return associated sprite as BufferedImage
     */
    public BufferedImage getSprite();
    
    /**
     * @param damageTaken (hit points lost as int)
     * reduces hit points by damageTaken
     */
    public void takeDamage(int damageTaken);

    /**
     * @return pokemon stats as string
     */
    public String displayStats();

    /**
    * @return pokemon moves as string
    */
    public String displayMovesString();

    /**
     * @param target (pokemon to target with moves as Pokemon)
     * Sets pokemons target to target
     */
    public void setTarget(Pokemon target);

}
