package no.uib.inf101.sem2.model.move;

public class BiteMove extends Move {
    //Constructor
    public BiteMove() {
        super(
            "Bite",
            "PHY",
            100,
            10, 
            25, 
            50, 
            20
        );
    }
    
}
