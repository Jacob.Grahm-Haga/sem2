package no.uib.inf101.sem2.model.move;

public class SlamMove extends Move {
    //Constructor
    public SlamMove() {
        super(
            "Slam",
            "PHY",
            75, 
            75, 
            100,
            25, 
            5
        );
    }
    
}
