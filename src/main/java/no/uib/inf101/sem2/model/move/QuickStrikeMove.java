package no.uib.inf101.sem2.model.move;

public class QuickStrikeMove extends Move {
    //Constructor
    public QuickStrikeMove() {
        super(
            "Quick-strike",
            "PHY",
            50,
            100, 
            0, 
            100, 
            10
        );
    }
    
}
