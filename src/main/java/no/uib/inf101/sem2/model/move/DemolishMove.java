package no.uib.inf101.sem2.model.move;

public class DemolishMove extends Move {
    //Constructor
    public DemolishMove() {
        super(
            "Demolish",
            "PHY",
            25, 
            200, 
            0,
            0, 
            5
        );
    }
    
}
