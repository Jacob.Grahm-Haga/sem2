package no.uib.inf101.sem2.model.battle;

import no.uib.inf101.sem2.model.pokemon.*;
import java.util.Random;
import java.util.ArrayList;

public class Battle implements IBattle {
    //Field vars
    Pokemon pokemon1;
    Pokemon pokemon2;
    int pokemonIndex1;
    int pokemonIndex2;
    ArrayList<Pokemon> pokemonList = new ArrayList<>();
    Random random;

    //Constructor
    public Battle(){
        pokemonList.add(new FredrikingPokemon());
        pokemonList.add(new IdrissaurPokemon());
        pokemonList.add(new MagnussaurousPokemon());
        pokemonList.add(new RebekkanitePokemon());
        pokemonList.add(new TobiassproutPokemon());
        pokemonList.add(new VincentiorPokemon());

        this.random = new Random();

        getRandomPokemonNums();
        
        this.pokemon1 = pokemonList.get(pokemonIndex1);
        this.pokemon2 = pokemonList.get(pokemonIndex2);
        pokemon1.setTarget(pokemon2);
        pokemon2.setTarget(pokemon1);
    }

    //Methods
    private void getRandomPokemonNums(){
        pokemonIndex1 = random.nextInt(pokemonList.size());
        pokemonIndex2 = random.nextInt(pokemonList.size());
        while (pokemonIndex1 == pokemonIndex2){ //get new random num until non duplicate
            pokemonIndex2 = random.nextInt(pokemonList.size());
        }
    } 

    @Override
    public Pokemon getPokemon1(){
        return pokemon1;
    }

    @Override
    public Pokemon getPokemon2(){
        return pokemon2;
    }

}
