package no.uib.inf101.sem2.model.move;

public class FeintMove extends Move {
    //Constructor
    public FeintMove() {
        super(
            "Feint",
            "PHY",
            100, 
            0, 
            0,
            100, 
            20
        );
    }
    
}
