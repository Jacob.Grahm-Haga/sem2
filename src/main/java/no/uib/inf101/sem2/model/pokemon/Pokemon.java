package no.uib.inf101.sem2.model.pokemon;

import java.lang.Math;
import java.util.*;
import no.uib.inf101.sem2.model.move.*;
import java.awt.image.BufferedImage;

public abstract class Pokemon implements IPokemon {
    //Field Variables
    String name;
    int HP;
    int maxHP;
    int speed;
    int attack;
    int specialAttack;
    int defense;
    int specialDefense;
    ArrayList<Move> moves;
    Pokemon target;
    BufferedImage sprite;
    Random random;

    //Constructor
    public Pokemon(String name, int HP, int speed, int attack, int specialAttack, int defense, int specialDefense, ArrayList<Move> moves, BufferedImage sprite) {
        this.name = name;
        this.HP = HP;
        this.maxHP = this.HP;
        this.speed = speed;
        this.attack = attack;
        this.specialAttack = specialAttack;
        this.defense = defense;
        this.specialDefense = specialDefense;
        this.moves = moves;
        this.sprite = sprite;
        this.random = new Random();
    }

    //Overrides
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getAttack() {
        return attack;
    }

    @Override
    public int getSpecialAttack() {
        return specialAttack;
    }

    @Override
    public int getDefense() {
        return defense;
    }

    @Override
    public int getSpecialDefense() {
        return specialDefense;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public int getCurrentHP() {
        return HP;
    }

    @Override
    public int getMaxHP() {
        return maxHP;
    }

    @Override
    public boolean isAlive() {
        return (HP > 0);
    }

    @Override
    public ArrayList<Move> getMoves() {
        return moves;
    }

    @Override
    public BufferedImage getSprite(){
        return sprite;
    }

    @Override
    public void takeDamage(int damageTaken) {
        HP -= Math.abs(damageTaken);
        if (HP <= 0) {
            HP = 0;
        }
    }

    @Override
    public void setTarget(Pokemon target){
        this.target = target;
    }

    @Override
    public String displayStats() {
        return ("\n" + name + "(" + HP + "/" + maxHP + "):\nSPD: " + speed + "\nATK: " + attack + " | SPE. ATK: " + specialAttack + "\nDEF: " + defense + " | SPE. DEF: " + specialDefense + "\n");
    }

    @Override
    public String displayMovesString() {
        String resultString = "";
        for (int moveNum = 0; moveNum < moves.size(); moveNum++) {
            resultString += moveNum + 1 + ". " + moves.get(moveNum).displayMove() + "\n";
        };
        return resultString;
    }
}