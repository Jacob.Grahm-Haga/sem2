package no.uib.inf101.sem2.model.pokemon;

import java.util.*;
import no.uib.inf101.sem2.view.*;

import no.uib.inf101.sem2.model.move.*;

public class MagnussaurousPokemon extends Pokemon{

    public MagnussaurousPokemon() {
        super(
            "Magnussaurous", 
            400, 
            50, 
            100, 
            50, 
            75, 
            25, 
            new ArrayList<>(Arrays.asList(
                new BiteMove(), 
                new SurgeMove(), 
                new DemolishMove(), 
                new SlamMove()
            )),
            Inf101Graphics.loadImageFromResources("/MagnussaurousSprite.png")
        );
    }
    
}
