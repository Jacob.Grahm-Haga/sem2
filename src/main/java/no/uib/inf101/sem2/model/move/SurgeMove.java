package no.uib.inf101.sem2.model.move;

public class SurgeMove extends Move {
    //Constructor
    public SurgeMove() {
        super(
            "Surge",
            "SPE",
            100, 
            25, 
            100,
            100, 
            10
        );
    }
    
}
