package no.uib.inf101.sem2.model.battle;
import no.uib.inf101.sem2.model.pokemon.Pokemon;

public interface IBattle {
    /**
     * @return pokemon1 as Pokemon 
     */
    public Pokemon getPokemon1();

    /**
     * @return pokemon2 as Pokemon 
     */
    public Pokemon getPokemon2();
}
