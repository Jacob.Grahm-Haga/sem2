package no.uib.inf101.sem2.model.pokemon;

import java.util.*;
import no.uib.inf101.sem2.view.*;

import no.uib.inf101.sem2.model.move.*;

public class FredrikingPokemon extends Pokemon{

    public FredrikingPokemon() {
        super(
            "Fredriking", 
            300, 
            75, 
            25, 
            100, 
            25, 
            75, 
            new ArrayList<>(Arrays.asList(
                new FeintMove(), 
                new SurgeMove(), 
                new QuickStrikeMove(), 
                new HyperBeamMove()
            )),
            Inf101Graphics.loadImageFromResources("/FredrikingSprite.png")
        );
    }
    
}
