package no.uib.inf101.sem2.model.move;
import no.uib.inf101.sem2.model.pokemon.Pokemon;

public interface IMove {
    /**
     * @return move name as string
     */
    public String getMoveName();

    /**
     * @return move hit chance as int (0-100)
     */
    public int getMoveHitChance();

    /**
     * @return move power/damage as int
     */
    public int getMovePower();

    /**
     * @return move speed as int (0-100)
     */
    public int getMoveSpeed();

    /**
     * @return max move count as int
     */
    public int getMaxMoveCount();

    /**
     * @return current move count as int
     */
    public int getMoveCount();

    /**
     * @return move stats as string
     */
    public String displayMoveString();

    /**
     * @return move stats in two segments as string array
     */
    public String[] displayMove();

    /**
     * if moveCount > 0, decrement moveCount and calls attack() with move
     * @return if move hit and damage done as string
     */
    public String useMove(Pokemon target);

    //Checks if move hit (random number (0-100) under hit%), and deals damage to target if it hit. If the random number is under 10% of hit% deals critical damage(2x). Returns string stating wether the attack hit and how much dmg target takes
    /**
     * Checks if move hit (random number (0-100) under move hit%).
     * if hit under 10% of move hit% deal 2x damage (critical hit)
     * if hit deal damage
     * if not hit deal no damage 
     * @return if move hit and damage done as string
     */
    public String attack(Pokemon target);
}
