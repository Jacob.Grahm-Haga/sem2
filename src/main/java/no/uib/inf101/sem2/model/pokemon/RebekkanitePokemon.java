package no.uib.inf101.sem2.model.pokemon;

import java.util.*;
import no.uib.inf101.sem2.view.*;

import no.uib.inf101.sem2.model.move.*;

public class RebekkanitePokemon extends Pokemon{

    public RebekkanitePokemon() {
        super(
            "Rebekkanite", 
            300, 
            100, 
            75, 
            75, 
            25, 
            25, 
            new ArrayList<>(Arrays.asList(
                new BiteMove(), 
                new SurgeMove(), 
                new KillCommandMove(), 
                new HyperBeamMove()
            )),
            Inf101Graphics.loadImageFromResources("/RebekkaniteSprite.png")
        );
    }
    
}
