package no.uib.inf101.sem2.model.pokemon;

import java.util.*;
import no.uib.inf101.sem2.view.*;

import no.uib.inf101.sem2.model.move.*;

public class VincentiorPokemon extends Pokemon{

    public VincentiorPokemon() {
        super(
            "Vincentior", 
            250, 
            100, 
            75, 
            75, 
            25, 
            25, 
            new ArrayList<>(Arrays.asList(
                new ScratchMove(), 
                new FuryMove(), 
                new KillCommandMove(), 
                new QuickStrikeMove()
            )),
            Inf101Graphics.loadImageFromResources("/VincentiorSprite.png")
        );
    }
    
}
