package no.uib.inf101.sem2.model.pokemon;

import java.util.*;
import no.uib.inf101.sem2.view.*;

import no.uib.inf101.sem2.model.move.*;

public class IdrissaurPokemon extends Pokemon{

    public IdrissaurPokemon() {
        super(
            "Idrissaur", 
            500, 
            25, 
            100, 
            0, 
            25, 
            0, 
            new ArrayList<>(Arrays.asList(
                new BiteMove(), 
                new SlamMove(), 
                new QuickStrikeMove(), 
                new DemolishMove()
            )),
            Inf101Graphics.loadImageFromResources("/IdrissaurSprite.png")
        );
    }
    
}
