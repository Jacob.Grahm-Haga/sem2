package no.uib.inf101.sem2.model.move;

public class KillCommandMove extends Move {
    //Constructor
    public KillCommandMove() {
        super(
            "Kill Command",
            "SPE",
            100, 
            50, 
            50,
            75, 
            10
        );
    }
    
}
