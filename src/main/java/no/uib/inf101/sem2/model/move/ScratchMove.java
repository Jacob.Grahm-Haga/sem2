package no.uib.inf101.sem2.model.move;

public class ScratchMove extends Move {
    //Constructor
    public ScratchMove() {
        super(
            "Scratch",
            "PHY",
            90, 
            50, 
            50,
            75, 
            15
        );
    }
    
}
