package no.uib.inf101.sem2.controller;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.sem2.model.battle.*;

public class TurnButton extends Button{
    //Field vars
    String text;
    Battle battle;

    //Constructor
    public TurnButton(Rectangle2D bounds, String text, Battle battle){
        super(bounds);
        this.text = text;
        this.battle = battle;
    }

    //Methods
    public String getText(){
        return text;
    }

    public Battle getBattle(){
        return battle;
    }
}
