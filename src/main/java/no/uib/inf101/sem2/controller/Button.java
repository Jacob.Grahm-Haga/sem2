package no.uib.inf101.sem2.controller;

import java.awt.geom.Rectangle2D;

public class Button {
    //Field vars
    Rectangle2D bounds;

    //Constructor
    public Button(Rectangle2D bounds){
        this.bounds = bounds;
    }

    public Rectangle2D getBounds(){
        return bounds;
    }
}
