package no.uib.inf101.sem2.controller;

import java.awt.geom.Rectangle2D;

public interface IButton {
    /**
     * @return bounds for button as Rectangle2D
     */
    public Rectangle2D getBounds();
}
