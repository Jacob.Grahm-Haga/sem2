package no.uib.inf101.sem2.controller;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.sem2.model.move.*;
import no.uib.inf101.sem2.model.pokemon.*;

public class MoveButton extends Button {
    //Field vars
    Move move;

    //Constructor
    public MoveButton(Rectangle2D bounds, Move move){
        super(bounds);
        this.move = move;
    }

    //Methods
    public Move getMove(){
        return move;
    }

    public String useMove(Pokemon target){
        return move.useMove(target);
    }
}
