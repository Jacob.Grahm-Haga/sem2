package no.uib.inf101.sem2;

import no.uib.inf101.sem2.view.*;
import no.uib.inf101.sem2.model.battle.Battle;

import javax.swing.JFrame;

public class Main {

  public static void main(String[] args) {
    //Initialize
    Battle battle = new Battle();

    View view = new View(battle);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Pokemon");
    frame.setContentPane(view);
    frame.pack();
    frame.setVisible(true);
  }
}
