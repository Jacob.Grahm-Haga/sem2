package no.uib.inf101.sem2.view;

import java.awt.geom.Rectangle2D;
import no.uib.inf101.sem2.controller.*;
import no.uib.inf101.sem2.controller.Button;
import no.uib.inf101.sem2.model.battle.*;
import no.uib.inf101.sem2.model.pokemon.*;
import javax.swing.JPanel;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

public class View extends JPanel{
    //Field vars
    Graphics2D g2;
    int windowWidth;
    int windowHeight;
    int frameMargin;
    int UIWidth;
    int UIHeightOffset;
    int gameWidth;
    int gameHeight;
    int textBoxHeight;
    boolean gameActive = true;
    Battle battle;
    Pokemon pokemon1;
    Pokemon pokemon2;
    ArrayList<Button> moveButtons1 = new ArrayList<>();
    ArrayList<Button> moveButtons2 = new ArrayList<>();
    boolean mouseOnButton = false;
    boolean mousePress = false;
    Button hoveringButton;
    Button activeButton1 = null;
    Button activeButton2 = null;
    TurnButton turnButton;
    Color uiColor = Color.DARK_GRAY;
    Color gameOverScreenColor = new Color(0,0,0,200);
    Color gameOverTextColor = Color.RED;
    Color textBoxColor = Color.WHITE;
    Color hoverButtonColor = Color.GRAY;
    Color activeButtonColor = Color.LIGHT_GRAY;
    Color unavailableButtonColor = Color.BLACK;
    Color backgroundColor = Color.CYAN;
    Color textColor = Color.WHITE;
    Color activeTextColor = Color.BLACK;
    Color unavailableTextColor = Color.GRAY;
    Color hpBarColor = Color.RED;
    Color missingHPBarColor = Color.BLACK;
    String textBoxString1 = "Then press [FIGHT!] to resolve the turn";
    String textBoxString2 = "Both players must choose a move";

    //Constructor
    public View(Battle battle) {
        this.windowWidth = 1000;
        this.windowHeight = 600;
        this.UIWidth = windowWidth/3;
        this.UIHeightOffset = windowHeight/10;
        this.frameMargin = windowWidth/100;
        this.gameWidth = windowWidth - 2 * frameMargin;
        this.gameHeight = windowHeight - 2 * frameMargin;
        this.textBoxHeight = windowHeight / 12;
        this.battle = battle;
        this.pokemon1 = battle.getPokemon1();
        this.pokemon2 = battle.getPokemon2();
        this.setPreferredSize(new Dimension(windowWidth, windowHeight));
        this.turnButton = new TurnButton(new Rectangle.Double(gameWidth/2 - UIWidth/2, frameMargin, UIWidth, textBoxHeight), "FIGHT!", battle);
        this.movesToButtons(moveButtons1, pokemon1, false);
        this.movesToButtons(moveButtons2, pokemon2, true);
        this.setupMousePositionUpdater();
        this.setupMousePressedUpdater();
      }

    //Overrides
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.g2 = (Graphics2D) g;
        updateCursor();
        drawGame();
    }

    private void movesToButtons(ArrayList<Button> moveButtons, Pokemon pokemon, boolean isRight){
        int buttonHeight = windowHeight/15;
        int buttonWidth = UIWidth/2;
        int moveIndex = 0;
        for (int row = 0; row < 2; row++) {
            for (int col = 0; col < 2; col++) {
                int buttonX = 2 * frameMargin + (col * (buttonWidth + frameMargin)) ;
                int buttonY = UIWidth + UIHeightOffset + buttonHeight + (row * (buttonHeight + frameMargin));
                if (isRight){
                    buttonX = gameWidth - (col * (buttonWidth + frameMargin)) - buttonWidth;
                }
                Rectangle2D button = new Rectangle2D.Double(buttonX, buttonY, buttonWidth, buttonHeight);
                moveButtons.add(new MoveButton(button, pokemon.getMoves().get(moveIndex)));
                moveIndex++;
            }
        }
    }

    //Methods
    private void drawText(Color color, Font font, String text, int x, int y, boolean rightAlign){
        g2.setColor(color);
        g2.setFont(font);
        if (rightAlign){
            FontMetrics fm = g2.getFontMetrics();
            x = gameWidth - fm.stringWidth(text);
        }
        g2.drawString(text, x, y);
    }

    private void drawRect(Rectangle2D rect, Color color){
        g2.setColor(color);
        g2.fill(rect);
    }

    private void drawGame() {
        //Draw frame
        Rectangle2D window = new Rectangle2D.Double(0, 0, windowWidth, windowHeight);
        drawRect(window, uiColor);
        
        //Draw game background
        Rectangle2D gameFrame = new Rectangle2D.Double(frameMargin, frameMargin, gameWidth, gameHeight - textBoxHeight - frameMargin);
        drawRect(gameFrame, backgroundColor);

        drawTextBox();

        drawTurnButton();

        //Draw pokemon 1
        drawPokemonName(pokemon1, false);
        drawPokemon(pokemon1, false);
        drawHPBar(pokemon1, false);
        drawMoves(moveButtons1);

        //Draw pokemon 2
        drawPokemonName(pokemon2, true);
        drawPokemon(pokemon2, true);
        drawHPBar(pokemon2, true);
        drawMoves(moveButtons2);

        //Draw game over screen
        if (!gameActive){
            drawRect(window, gameOverScreenColor);
            drawText(gameOverTextColor, new Font("Arial", Font.BOLD, gameHeight/15), "", 0, 0, false); //Dummy to change font/Color for centered string
            String text = "Draw!"; //Default case if neither are alive
            if (pokemon1.isAlive()){
                text = pokemon1.getName() + " wins!";
            } else if (pokemon2.isAlive()){
                text = pokemon2.getName() + " wins!";
            }
            Inf101Graphics.drawCenteredString(g2, text, window);
        }

    }

    private void drawTextBox(){
        Rectangle2D textBox = new Rectangle2D.Double(frameMargin, windowHeight - textBoxHeight - frameMargin, gameWidth, textBoxHeight);
        drawRect(textBox, textBoxColor);
        drawText(activeTextColor, new Font("Arial", Font.BOLD, gameHeight/30), textBoxString1, frameMargin * 2, gameHeight, true);
        drawText(activeTextColor, new Font("Arial", Font.BOLD, gameHeight/30), textBoxString2, frameMargin * 2, gameHeight - frameMargin * 2, false);
    }

    private void drawPokemonName(Pokemon pokemon, boolean isRight){
        int namePlateX = frameMargin;
        if (isRight){
            namePlateX = gameWidth - UIWidth/2 + frameMargin;
        }
        Rectangle2D namePlate = new Rectangle2D.Double(namePlateX, frameMargin , UIWidth/2, frameMargin * 3);
        drawRect(namePlate, uiColor);
        drawText(textColor, new Font("Arial", Font.BOLD, gameHeight/30), pokemon.getName(), frameMargin, frameMargin * 3, isRight);
    }

    private void drawTurnButton(){
        Color buttonColor = activeButtonColor;
        Color buttonTextColor = activeTextColor;
        if (!movesSelected()){
            buttonColor = unavailableButtonColor;
            buttonTextColor = unavailableTextColor;
        }
        drawRect(turnButton.getBounds(), buttonColor);
        drawText(buttonTextColor, new Font("Arial", Font.BOLD, gameHeight/15), "", 0, 0, false); //Dummy to change font/Color for centered string
        Inf101Graphics.drawCenteredString(g2, turnButton.getText(), turnButton.getBounds());
    }

    private void drawPokemon(Pokemon pokemon, boolean isRight){
        if (isRight){
            g2.drawImage(pokemon.getSprite(), gameWidth - UIWidth, UIHeightOffset, UIWidth, UIWidth, null);
        } else {
            g2.drawImage(pokemon.getSprite(), UIWidth + (frameMargin * 2), UIHeightOffset, -UIWidth, UIWidth, null);
        }
    }

    private void drawHPBar(Pokemon pokemon, boolean isRight){
        //Values
        int barHeight = windowHeight/30;
        int borderOffset = frameMargin * 2;
        int barHeightOffset = UIWidth + UIHeightOffset;
        int shadowPx = windowHeight/200;

        //Get current % HP
        double HPpercentage = (double) pokemon.getCurrentHP() / pokemon.getMaxHP();

         //Left or default 
        Rectangle2D maxHPBar = new Rectangle2D.Double(borderOffset, barHeightOffset + shadowPx, UIWidth + shadowPx, barHeight + shadowPx);
        Rectangle2D currentHPBar = new Rectangle2D.Double(borderOffset, barHeightOffset, UIWidth * HPpercentage, barHeight);
        
        //Right or flipped
        if (isRight){ 
            maxHPBar = new Rectangle2D.Double(gameWidth - UIWidth - shadowPx, barHeightOffset + shadowPx, UIWidth + shadowPx, barHeight + shadowPx);
            currentHPBar = new Rectangle2D.Double(gameWidth - (UIWidth * HPpercentage), barHeightOffset, UIWidth * HPpercentage, barHeight);
        }

        //Draw max HP / background
        drawRect(maxHPBar, missingHPBarColor);

        //draw current HP
        drawRect(currentHPBar, hpBarColor);

        drawHPNums(pokemon, borderOffset, barHeightOffset + barHeight, isRight);
    }

    private void drawHPNums(Pokemon pokemon, int x, int y, boolean isRight){
        String text = pokemon.getCurrentHP() + "/" + pokemon.getMaxHP();
        drawText(textColor, new Font("Arial", Font.BOLD, gameHeight/25), text, x, y, isRight);
    }

    private void drawMoves(ArrayList<Button> moveButtons){
        for (Button moveButton : moveButtons) {
            int buttonWidth = (int) moveButton.getBounds().getWidth();
            int buttonHeight = (int) moveButton.getBounds().getHeight();
            int buttonX = (int) moveButton.getBounds().getX();
            int buttonY = (int) moveButton.getBounds().getY();
            String[] text = ((MoveButton) moveButton).getMove().displayMove();
            Color moveButtonColor;
            Color moveTextColor = textColor;
            if ((moveButton == activeButton1) || (moveButton == activeButton2)){
                moveButtonColor = activeButtonColor;
                moveTextColor = activeTextColor;
            } else if (((MoveButton) moveButton).getMove().getMoveCount() <= 0) {
                moveButtonColor = unavailableButtonColor;
                moveTextColor = unavailableTextColor;
            } else if (moveButton == hoveringButton) {
                moveButtonColor = hoverButtonColor;
            } else {
                moveButtonColor = uiColor;
            }
            drawRect(moveButton.getBounds(), moveButtonColor);
            drawText(moveTextColor, new Font("Arial", Font.BOLD, buttonWidth/13), text[0], buttonX, buttonY + buttonHeight/3, false);
            drawText(moveTextColor, new Font("Arial", Font.PLAIN, buttonWidth/15), text[1], buttonX, buttonY + buttonHeight/2 + frameMargin, false);
        }
    }

    private void setupMousePositionUpdater() {
        // Keep the mousePosition variable up to date
        this.addMouseMotionListener(new MouseMotionAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                if (gameActive){
                    ArrayList<Button> allMoveButtons = new ArrayList<>();
                    allMoveButtons.addAll(moveButtons1);
                    allMoveButtons.addAll(moveButtons2);
                    for (Button moveButton : allMoveButtons) {
                        if (((MoveButton) moveButton).getMove().getMoveCount() > 0){
                            if (updateHovering(moveButton, e)){
                                repaint();
                                return;
                            }
                        }
                    }
                    if (movesSelected()){
                        if (updateHovering(turnButton, e)){
                            repaint();
                            return;
                        } 
                    }
                }
                hoveringButton = null;
                mouseOnButton = false;
                repaint();
            }
        });
    }

    private boolean updateHovering(Button button, MouseEvent e){
        boolean mouseOnButtonTemp = button.getBounds().contains(e.getPoint());
        mouseOnButton = mouseOnButtonTemp;
        if (mouseOnButtonTemp ){
            hoveringButton = button;
        }
        return mouseOnButtonTemp;
    }
    
    private void updateCursor() {
        if (mouseOnButton) {
          setCursor(new Cursor(Cursor.HAND_CURSOR));
        } else {
          setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
    }
    
    private void setupMousePressedUpdater() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mousePress = true;
                if ((moveButtons1.contains(hoveringButton))){
                    activeButton1 = hoveringButton;
                } else if (moveButtons2.contains(hoveringButton)){
                    activeButton2 = hoveringButton;
                } else if (turnButton == hoveringButton){
                    nextTurn();
                }
                repaint();
            }
      
            @Override
            public void mouseReleased(MouseEvent e) {
                mousePress = false;
                repaint();
            }
        });
    }

    private void nextTurn(){
        if (movesSelected()){
            textBoxString1 = ((MoveButton) activeButton1).useMove(pokemon2);
            activeButton1 = null;
            textBoxString2 = ((MoveButton) activeButton2).useMove(pokemon1);
            activeButton2 = null;
            if ((!pokemon1.isAlive()) || (!pokemon2.isAlive())){
                endGame();
            }
            repaint();
        }
    }

    private boolean movesSelected(){
        return ((activeButton1 != null) && (activeButton2 != null));
    }

    private void endGame(){
        gameActive = false;
    }
}


