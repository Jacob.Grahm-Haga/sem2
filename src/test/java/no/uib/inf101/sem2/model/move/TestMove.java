package no.uib.inf101.sem2.model.move;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import no.uib.inf101.sem2.model.pokemon.*;

public class TestMove {
    @Test
    public void testMoveCount() {
        Pokemon target = new FredrikingPokemon();
        Move move = new SlamMove();
        assertEquals(move.getMoveCount(), move.getMaxMoveCount()); //Assert move count starts at max
        move.useMove(target);
        assertEquals(move.getMoveCount(), move.getMaxMoveCount() - 1); //Assert move count decrements by 1
        move.useMove(target);
        move.useMove(target);
        move.useMove(target);
        assertNotEquals(move.useMove(target), "Out of moves");
        assertEquals(move.useMove(target), "Out of moves"); 
    }

    @Test
    public void testMoveDamage() {
        Pokemon target = new FredrikingPokemon();
        Move move = new BiteMove();
        assertEquals(target.getCurrentHP(), target.getMaxHP()); //Assert target HP starts at max
        move.useMove(target);
        assertNotEquals(target.getCurrentHP(), target.getMaxHP()); //Assert damage has been done
    }
}
