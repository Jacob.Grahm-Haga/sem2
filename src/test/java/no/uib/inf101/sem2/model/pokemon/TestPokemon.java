package no.uib.inf101.sem2.model.pokemon;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class TestPokemon {

    @Test
    public void testPokemonIsAlive() {
        Pokemon target = new FredrikingPokemon();
        assertEquals(target.getCurrentHP(), target.getMaxHP()); //Assert target HP starts at max
        target.takeDamage(290);
        assertEquals(target.getCurrentHP(), target.getMaxHP()-290); //Assert damage has been done
        assertTrue(target.isAlive()); //Checks if pokemon is alive at 10hp
        target.takeDamage(290); //Takes more dmg than hp
        assertFalse(target.isAlive()); //Asserts that pokemon is not alive
        assertTrue(target.getCurrentHP() == 0); //Asserts that pokemon hp bottoms out at 0 and does not go negative
    }
}

